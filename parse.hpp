#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<string> parse_line(string line);
vector<vector<string> > read_file(string file_name);
vector<vector<double> > to_double(vector<vector<string> > data);
bool is_num(string & s);
