#include <iostream>
#include <vector>
#include <string>
#include <Eigen/Dense>
#include <cstdlib>
#include <iomanip>
#include <unistd.h>
#include "parse.hpp"

using namespace std;

double cal_vol(Eigen::MatrixXd cov_mat, Eigen::MatrixXd weights)
{
    Eigen::MatrixXd weightsT = weights.transpose();
    Eigen::MatrixXd vol = weightsT * cov_mat * weights;
    return sqrt(vol(0,0));
}

Eigen::MatrixXd cal_unrestricted_weights(Eigen::MatrixXd cov_mat, Eigen::MatrixXd A, double expcted_ret, vector<int> flags = vector<int>())
{
    int n = cov_mat.rows();
    Eigen::MatrixXd zeros = Eigen::MatrixXd::Zero(2,2);
    Eigen::MatrixXd AT = A.transpose();
    Eigen::MatrixXd factor(n + 2, n + 2 );
    Eigen::MatrixXd b(2, 1);
    b(0, 0) = 1; 
    b(1, 0) = expcted_ret;
    factor<< cov_mat, AT,
             A, zeros;
    Eigen::MatrixXd right(n + 2, 1);
    Eigen::MatrixXd zeros_temp = Eigen::MatrixXd::Zero(n, 1);
    right << zeros_temp,
          b;
    Eigen::MatrixXd solution;
    if(flags.size() == 0)
    {
        solution = factor.lu().solve(right);
    }
    else
    {
        int original_rows = factor.rows();
        int original_cols = factor.cols();
        factor.conservativeResize(factor.rows() + flags.size(), factor.cols() + flags.size());
        for(unsigned i = 0; i<flags.size(); i++)
        {
            factor.row(original_rows + i).setZero();
            factor.col(original_cols + i).setZero();
            factor(original_rows + i, flags[i]) = 1;
            factor(flags[i], original_cols + i) = 1;
        }
        right.conservativeResize(right.rows() + flags.size(), right.cols());
        for( int i = flags.size(); i>0; i--)
        {
            right(right.rows() - i, 0) = 0;
        }
        solution = factor.lu().solve(right);
    }
   
    Eigen::MatrixXd weights = solution.topRows(n); 
    return weights;
}

int find_min_weight(Eigen::MatrixXd target)
{
    int n = target.rows();
    int re = 0;
    //cout<<target<<endl;
    for(int  i = 0; i<n; i++)
    {
        if(target(i, 0) < target(re, 0))
        {
            re = i;
        }
    }
    return re;
}

Eigen::MatrixXd cal_restricted_weights(Eigen::MatrixXd cov_mat, Eigen::MatrixXd A, double expcted_ret)
{
    Eigen::MatrixXd weights = cal_unrestricted_weights(cov_mat, A, expcted_ret);
    int min_pos = find_min_weight(weights);
    vector<int> flags;
    while(weights(min_pos, 0) < -1e-9)
    {
        flags.push_back(min_pos);
        weights = cal_unrestricted_weights(cov_mat, A, expcted_ret, flags);
        min_pos = find_min_weight(weights);
    }
    //cout<<weights<<endl;
    return weights;
} 

int main(int argc, char** argv)
{
    if(argc != 3 && argc != 4)
    {
        cerr<<"Must have at least 2 arguments plus at most one option."<<endl;
        return EXIT_FAILURE;
    }

    string file1;
    string file2;
    bool is_stricted = false;
    if(argc == 3)
    {
        file1 = string(argv[1]);
        file2 = string(argv[2]);
      
    }
    else
    {
        //string opt = argv[1];
        char opt = 0;
        while((opt = getopt(argc, argv, "r")) != -1)
        {
            switch(opt)
            {
                case 'r':
                    is_stricted = true;
                    break;
            }
        }
        if(is_stricted != true)
        {
            cerr<<"invalid option: "<<opt<<endl;
            return EXIT_FAILURE;
        }
        else
        {
            file1 = string(argv[2]);
            file2 = string(argv[3]);
        }

    }
    double rp = 0.02;
    vector<vector<string> > v = read_file(file1);
    Eigen::MatrixXd ones(1, v.size());
    Eigen::MatrixXd avg_rtn(1, v.size());
    
    Eigen::VectorXd stdev(v.size(), 1);
    
    
    for(int i = 0; i<v.size(); i++)
    {
        v[i].erase(v[i].begin());
        if(v[i].size() != 2)
        {
            cerr<<"Wrong size of input file1."<<endl;
            exit(EXIT_FAILURE);
        }
    }
    vector<vector<double> > dv = to_double(v);
    unsigned int dim = dv.size();
    for(int i = 0; i<dv.size(); i++)
    {
        avg_rtn(0, i) = dv[i][0];
        stdev(i) = dv[i][1];
        ones(0, i) = 1;
    }
    v = read_file(file2);
    if(v.size() != dv.size())
    {
        cerr<<"Wrong size of input file2."<<endl;
        exit(EXIT_FAILURE);
    }
    for(int i = 0; i<v.size(); i++)
    {
        if(v[i].size() != v.size())
        {
            cerr<<"Wrong size of input file2."<<endl;
            exit(EXIT_FAILURE);
        }
    }
    vector<vector<double> > corr = to_double(v);
    Eigen::MatrixXd corr_mat(dim, dim);
    for(unsigned int i = 0; i<dim; i++)
    {
        for(unsigned int j = 0; j<dim; j++)
        {
            corr_mat(i, j) = corr[i][j];
        }
    }
    Eigen::MatrixXd cov_mat = Eigen::MatrixXd(corr_mat.rows(), corr_mat.cols());
    for(unsigned i = 0; i<dim; i++)
    {
        for(unsigned j = 0; j<dim; j++)
        {
            cov_mat(i, j) = corr_mat(i, j)*stdev(i)*stdev(j);
        }
    }
    Eigen::MatrixXd A(2, dim);
    A<<ones,
       avg_rtn;
    for(unsigned i = 0; i<dim; i++)
    {
        for(unsigned j = 0; j<dim; j++)
        {
            cov_mat(i, j) = corr_mat(i, j)*stdev(i)*stdev(j);
        }
    }
    cout<<"ROR,volatility"<<endl;
    cout<<fixed<<setprecision(2);
    if(!is_stricted)
    {
        for(int d = 1; d<=26; d ++)
        {
            Eigen::MatrixXd weights = cal_unrestricted_weights(cov_mat, A, ((double)d)/100);
            double vol = cal_vol(cov_mat, weights);
            cout<<fixed<<setprecision(1)<<(double)d<<"%,"<<fixed<<setprecision(2)<<vol*100<<"%"<<endl;
        }
    }
    else
    {
        for(int d = 1; d<=26; d ++)
        {
            Eigen::MatrixXd weights = cal_restricted_weights(cov_mat, A, d/100.00);
            double vol = cal_vol(cov_mat, weights);
            //cout<<(double)d<<"%,"<<vol*100<<"%"<<endl;
            cout<<fixed<<setprecision(1)<<(double)d<<"%,"<<fixed<<setprecision(2)<<vol*100<<"%"<<endl;
           // cout<<d<<":"<<weights.transpose()<<endl;
        }
    } 
    // Eigen::MatrixXd weights = cal_restricted_weights(cov_mat, A, 14/100.0);
    // double vol = cal_vol(cov_mat, weights);
    // cout<<14<<"%,"<<vol*100<<"%"<<endl;

    return 0;
}
