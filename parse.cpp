#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>

#include "parse.hpp"

using namespace std;

vector<string> parse_line(string line)
{
	vector<string> re;
	vector<size_t> delims;
	size_t pos = 0;
	while((pos = line.find(",", pos)) != string::npos)
	{
		delims.push_back(pos);
		pos ++;
	}
	pos = 0;
	string temp;
	for(unsigned int i = 0; i<delims.size(); i++)
	{
		temp = line.substr(pos, delims[i] - pos);
		pos = delims[i] + 1;
		re.push_back(temp);
	}
	if(delims.size() == 0)
	{
		return vector<string>();
	}
	pos = delims[delims.size() - 1];
	temp = line.substr(pos + 1, line.length() - pos);
	re.push_back(temp);
	return re;
}

vector<vector<string> > read_file(string file_name)
{
	vector<vector<string> > re;
	fstream fs;
	fs.open(file_name, fstream::in);
	string title;
	string line;
	if(!getline(fs, title))
	{
		cerr<<"Fail in getting the first line."<<endl;
		exit(EXIT_FAILURE);
	}
	vector<string> vtitle = parse_line(title);
	re.push_back(vtitle);

	while(getline(fs, line))
	{
		vector<string> temp = parse_line(line);
		re.push_back(temp);
	}
	return re;
}

vector<vector<double> > to_double(vector<vector<string> > data)
{
	vector<vector<double> > re(data.size(), vector<double>());
	//vector<double> prior_valid(data[0].size() - 1, 0);
	for(unsigned int i = 0; i<data.size(); i++)
	{
		for(unsigned int j = 0; j<data[i].size(); j++)
		{
			    char *temp = NULL;
				double num = strtod(data[i][j].c_str(), &temp);
				if(*temp != 0)
				{
					cerr<<"Not a number."<<endl;
					exit(EXIT_FAILURE);
				}
				/*stringstream ss;
				ss << data[i][j];
				ss >> num;*/
				// if(j - 1 > re.size() - 1)
				// {
				// 	continue;
				// }
				re[i].push_back(num);
		}
	}

	return re;
}

